﻿using System;

using uplink.NET.Models;
using uplink.NET.Services;
using System.Threading.Tasks;

namespace UplinkDemo
{
    class Program
    {
        static async Task<int> Main(string[] args)
        {
            Access.SetTempDirectory(System.IO.Path.GetTempPath());
            var config = new Config();
            config.UserAgent = "uplink-csharp";
            config.DialTimeoutMilliseconds = 5000;
            config.TempDirectory = System.IO.Path.GetTempPath();

            var access = new Access(/* credentials */);

            var bucketService = new BucketService(access);
            ListBucketsOptions listOptions = new ListBucketsOptions();
            var buckets = await bucketService.ListBucketsAsync(listOptions);
            foreach (var bucket in buckets.Items) {
                Console.WriteLine(bucket.Name);
            }
            

            return 0;
        }
    }
}
